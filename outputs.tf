# endpoint outputs
output "rds_prod_endpoint" {
  value = "${aws_db_instance.database-wp.address}"
}
output "ELB_endpoint" {
  value = aws_elb.elb-wordpress.dns_name
}
