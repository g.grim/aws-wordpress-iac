# HA Wordpress in AWS using Terraform
A full suite to install a high-availability WordPress installation on AWS using Terraform.

prerequisites:
- an AWS account 
- AWS credentials saved on the local machine using the AWS CLI functionality 
- create the keypair used by this repo running into the folder this command: "ssh-keygen -f mykey"

Will create:
  - Virtual Private Cloud (VPC) with different az configurations to prevent single point of failure and to provide high availability.
  - Security Groups (SG)
  - Relational Database Service (RDS) to store our data: multi az deployment config to prevent failures and to provide high availability
  - Auto-Scaling Group (ASG) to create a scalable, high availability cluster
  - Elastic Load Balancer (ELB) to balance incoming traffic
  - Automatic upload of "wp-config.php" using a data rendered template which provides the db-endpoint. 

Steps:
  - Create your keypair in folder. Run: ssh-keygen -f mykey
  - Run Terraform
  - It will output ELB and RDS endpoints. Connect to the ELB endpoint to complete the Wordpress install. Please note that ELB takes some time to run, so wait a bit in case of blank response. Eventually, once the health checks are over, WP install will start.
  - You now have a fully functioning HA Wordpress suite. 

