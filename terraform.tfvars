region                            = "eu-south-1"   //regione Milano


# Variabili launch/EC2
scaling_ami                           = "ami-09866aeb841e3b5eb"
instance_type                         = "t3.micro"
scaling_min                      = "1"
scaling_max                      = "3"
scaling_desired                 = "2"
user_data_path                   = "./scripts/script-wpsetup.sh"   //path dello sript installazione wp 

# variabili delle chiavi 
PATH_TO_PRIVATE_KEY                   = "mykey"
PATH_TO_PUBLIC_KEY                    = "mykey.pub"


#DB-RDS variabili
rds_instance_class = "db.t3.micro"
db_username = "test"
db_password = "test123$%"

#VPC variabili
az1 = "eu-south-1a"
az2= "eu-south-1b"
az3 = "eu-south-1c"