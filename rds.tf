resource "aws_db_instance" "database-wp" {
  db_name                        = "Wordpressdb"
  allocated_storage           = 10
  engine                      = "mysql"
  engine_version              = "5.7"
  instance_class              = var.rds_instance_class
  backup_retention_period     = 7
  publicly_accessible         = "true"
  username                    = var.db_username
  password                    = var.db_password
  vpc_security_group_ids      = [aws_security_group.db-sg.id]
  db_subnet_group_name        = aws_db_subnet_group.db-subnet-group.name
  parameter_group_name        = "default.mysql5.7"
  multi_az                    = "true"
  skip_final_snapshot         = "true"
  identifier = "mysql-wp"
}


resource "aws_db_subnet_group" "db-subnet-group" {
    name          = "db-subnet-group"
    subnet_ids    = [aws_subnet.main-private-1.id, aws_subnet.main-private-2.id,aws_subnet.main-private-3.id]
}
