#variabili provider
variable "region" {}

# variabili chiavi
variable "PATH_TO_PRIVATE_KEY" {}
variable "PATH_TO_PUBLIC_KEY" {}

#variabili launch config e autoscaling group 
variable "scaling_ami" {}     
variable "instance_type" {}
variable "scaling_min" {}
variable "scaling_max" {}
variable "scaling_desired" {}
variable "user_data_path"  {}

#variabili del DB
variable "rds_instance_class" {}
variable "db_username" {}
variable "db_password" {}

#variabili VPC
variable "az1" {}
variable "az2" {}
variable "az3" {}
