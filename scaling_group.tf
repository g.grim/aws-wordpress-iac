#data template per lo script di installazione di wp
data "template_file" "phpconfig" {
  template = file("${path.module}/scripts/script-wpsetup.sh")
  vars = {
    db_host = "${aws_db_instance.database-wp.address}"
  }
}



resource "aws_launch_configuration" "lc-wordpress" {
  name_prefix     = "launch_conf_wp"
  image_id        = var.scaling_ami
  instance_type   = var.instance_type
  key_name        = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.ec2-sg.id]
  user_data       = data.template_file.phpconfig.rendered
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg-wordpress" {
  name                      = "asg-wordpress"
  vpc_zone_identifier       = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
  launch_configuration      = aws_launch_configuration.lc-wordpress.name
  min_size                  = var.scaling_min
  max_size                  = var.scaling_max
  desired_capacity          = var.scaling_desired
  health_check_grace_period = 300
  health_check_type         = "ELB"
  load_balancers            = [aws_elb.elb-wordpress.name]
  force_delete              = true

  tag {
    key                 = "Name"
    value               = "WP-EC2"
    propagate_at_launch = true
  }
}
